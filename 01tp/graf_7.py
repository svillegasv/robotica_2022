import numpy as np
import matplotlib.pyplot as plt

file = 'log_10_1.txt'

##### Configurables #####
index_a   = 1000                # Indice del primer  punto a graficar
i_min = 70                   # Limite inferior de los datos a graficar
i_max = 1046                # Limite superior de los datos a graficar
offset = 50                 #offset destinado a modificar la visualizacion de las cajitas con los datos


bbox = dict(boxstyle ="round",ec = "0",fc ="0.7")
arrowprops = dict(facecolor='grey',arrowstyle = "->",connectionstyle = "arc,angleA=0,armA=50,rad=10")

##### Levanta la tabla en listas #####
tiempo      = np.genfromtxt(file, usecols = 0, dtype = float) 
pos_x       = np.genfromtxt(file, usecols = 1, dtype = float)   
pos_y       = np.genfromtxt(file, usecols = 2, dtype = float)   

##### Plot de tryectoria #####
plt.subplot(2, 2, 1)
plt.plot(pos_x[i_min:i_max],pos_y[i_min:i_max],'-b')
plt.title("Camino recorrido")
plt.xlabel('X [m]')
plt.ylabel('Y [m]')
plt.axis('equal')
plt.grid()

plt.scatter(pos_x[index_a], pos_y[index_a], color = 'b')

plt.annotate('(%.1f, %.1f)'%(pos_x[index_a], pos_y[index_a]),
            (pos_x[index_a], pos_y[index_a]), xytext =(-2*offset,  0.5 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)

##### Plot posicion en funcion de tiempo #####
plt.subplot(2, 2, 2)
plt.plot(tiempo[i_min:i_max],pos_x[i_min:i_max],'-b',label = 'Pos. en X')
plt.plot(tiempo[i_min:i_max],pos_y[i_min:i_max],'-g',label = 'Pos. en Y')
plt.title("Posición en X e Y")
plt.xlabel('Tiempo [s]')
plt.ylabel('Posición')
plt.legend()
plt.grid()
plt.scatter(tiempo[index_a],pos_y[index_a], color = 'r')

plt.annotate('(%.1f, %.1f)'%(tiempo[index_a],pos_y[index_a]),
            (tiempo[index_a],pos_y[index_a]), xytext =(1*offset,  0.5 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)

plt.scatter(tiempo[index_a],pos_x[index_a], color = 'r')
plt.annotate('(%.1f, %.1f)'%(tiempo[index_a],pos_x[index_a]),
            (tiempo[index_a],pos_x[index_a]), xytext =(-2*offset,  0.5 * offset),
            textcoords ='offset points',
            bbox = bbox, arrowprops = arrowprops)

plt.show()
