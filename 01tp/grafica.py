import numpy as np
import matplotlib.pyplot as plt

file = 'log.txt'

tiempo, orientacion= np.loadtxt(file, usecols= [0,3], unpack= True )
x, y= np.loadtxt(file, usecols= [1,2], unpack= True )
vel_lin= np.loadtxt(file, usecols= [4], unpack= True )
vel_ang = np.loadtxt(file, usecols= [5], unpack= True )

# tiempo (timestamp), coordenadas x, y, orientacion, velocidad lineal y angular

#camino seguido por el robot
plt.subplot(2,2,1)
plt.plot(x,y, 'r--', data= float, label='Posición')
plt.title("Posicion")
plt.xlabel("Posicion de x")
plt.ylabel("Poscion de y")
plt.grid(True)
plt.legend()
plt.axis('equal')  #el parametro 'equal' matiene la relacion de aspecto para no deformar la imagen

#trayectoria
plt.subplot(2,2,2)
plt.plot(tiempo, orientacion)
plt.title("Trayectoria")
plt.xlabel("Tiempo")
plt.ylabel("Orientacion")
plt.grid(True)
plt.legend()

#velocidad respecto al tiempo
plt.subplot(2,2,3)
plt.plot(tiempo,vel_lin, label='vel lineal')
plt.plot(tiempo,vel_ang, label='vel angular')
plt.title("Velocidad")
plt.xlabel("Tiempo")
plt.ylabel("Vel")
plt.grid(True)
plt.legend()

plt.show()